package net.voronoff.beforeafter.filters;

import android.os.Parcel;
import android.os.Parcelable;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSepiaFilter;

public class FiltersSettings implements Parcelable {
    private int sepia;
    private int blur;

    protected FiltersSettings(Parcel in) {
        sepia = in.readInt();
        blur = in.readInt();
    }

    public static final Creator<FiltersSettings> CREATOR = new Creator<FiltersSettings>() {
        @Override
        public FiltersSettings createFromParcel(Parcel in) {
            return new FiltersSettings(in);
        }

        @Override
        public FiltersSettings[] newArray(int size) {
            return new FiltersSettings[size];
        }
    };

    public FiltersSettings() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sepia);
        dest.writeInt(blur);
    }

    public int getSepia() {
        return sepia;
    }

    public void setSepia(int sepia) {
        this.sepia = sepia;
    }

    public int getBlur() {
        return blur;
    }

    public void setBlur(int blur) {
        this.blur = blur;
    }
}
