package net.voronoff.beforeafter.filters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.util.Log;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;


public class BitmapProcessor {
    private static final String TAG = "BitmapProcessor";

    private Bitmap bitmap;
    private boolean split;
    private GPUImage gpuImage;
    private GPUImageFilter filter;

    public BitmapProcessor(Context context) {
        this(context, null, true);
    }

    public BitmapProcessor(Context context, Bitmap bitmap, boolean split) {
        this(context, bitmap, split, null);
    }

    public BitmapProcessor(Context context, Bitmap bitmap, boolean split, GPUImageFilter filter) {
        gpuImage = new GPUImage(context);
        this.bitmap = bitmap;
        this.split = split;
        this.filter = filter;
    }

    public synchronized Bitmap process(int width, int height) {
        if (width <= 0 || height <= 0) {
            Log.w(TAG, "wrong size");
            return null;
        }

        if (bitmap == null) {
            Log.w(TAG, "bitmap is null");
            return null;
        }
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();

        Bitmap resultBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawColor(Color.TRANSPARENT);

        float scaleFactor = Math.min((float) width / bitmapWidth, (float) height / bitmapHeight);

        int scaledBitmapWidth = (int) (bitmapWidth * scaleFactor);
        int scaledBitmapHeight = (int) (bitmapHeight * scaleFactor);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, scaledBitmapWidth, scaledBitmapHeight, false);

        float vTrans = 0f;
        float hTrans = 0f;
        if (width > scaledBitmapWidth)
            vTrans = (width - scaledBitmapWidth) / 2;
        if (height > scaledBitmapHeight)
            hTrans = (height - scaledBitmapHeight) / 2;

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        canvas.drawBitmap(scaledBitmap, vTrans, hTrans, paint);

        if (filter == null)
            return resultBitmap;

        gpuImage.setFilter(filter);
        Bitmap filteredBitmap = gpuImage.getBitmapWithFilterApplied(scaledBitmap);
        if (split) {
            Rect src = new Rect(scaledBitmapWidth / 2, 0, scaledBitmapWidth, scaledBitmapHeight);
            RectF dst = new RectF(vTrans + scaledBitmapWidth / 2, hTrans,
                    vTrans + scaledBitmapWidth, height - hTrans);
            canvas.drawBitmap(filteredBitmap, src, dst, paint);

            // Draw vertical line
            Paint linePaint = new Paint();
            linePaint.setColor(Color.WHITE);
            linePaint.setStrokeWidth(2);
            canvas.drawLine(width / 2, hTrans, width / 2, height - hTrans, linePaint);
        } else {
            canvas.drawBitmap(filteredBitmap, vTrans, hTrans, paint);
        }

        return resultBitmap;
    }

    public void processAsync(final OnBitmapReadyListener listener, final int width, final int height) {
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                return process(width, height);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                listener.ready(bitmap);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public synchronized void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public synchronized void setSplit(boolean split) {
        this.split = split;
    }

    public synchronized void setFilter(GPUImageFilter filter) {
        this.filter = filter;
    }

    public interface OnBitmapReadyListener {
        void ready(Bitmap bitmap);
    }
}
