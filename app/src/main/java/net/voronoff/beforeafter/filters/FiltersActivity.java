package net.voronoff.beforeafter.filters;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;

import net.voronoff.beforeafter.R;
import net.voronoff.beforeafter.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageBoxBlurFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageGrayscaleFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSepiaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSobelEdgeDetection;

public class FiltersActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener,
        AdapterView.OnItemSelectedListener, TabLayout.OnTabSelectedListener {
    private static final String TAG = "FiltersActivity";

    public static final String ARG_PATH = "ARG_PATH";

    private static final int TAB_BERORE_AND_AFTER = 0;
    private static final int TAB_BERORE           = 1;
    private static final int TAB_AFTER            = 2;

    private static final int FILTER_SEPIA = 0;
    private static final int FILTER_BLUR  = 1;
    private static final int FILTER_GRAY  = 2;
    private static final int FILTER_EDGE  = 3;

    private static final String STATE_TAB     = "STATE_TAB";
    private static final String STATE_FILTERS = "STATE_FILTERS";

    private TabLayout tabLayout;
    private BitmapSurfaceView bitmapSurfaceView;
    private RelativeLayout rlControls;
    private SeekBar sbIntensity;
    private Spinner spinner;

    private BitmapProcessor bitmapProcessor;

    private FiltersSettings filtersSettings;

    private GPUImageSepiaFilter sepiaFilter;
    private GPUImageBoxBlurFilter boxBlurFilter;
    private GPUImageGrayscaleFilter grayscaleFilter;
    private GPUImageSobelEdgeDetection edgeDetection;

    private boolean ready = true;
    private boolean dirty = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filters);

        Intent intent = getIntent();
        Bitmap bitmap = Utils.decodeBitmapDisplaySize(this, intent.getStringExtra(ARG_PATH));

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.tab_title_BeforeAndAfter)));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_title_Before));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_title_After));
        tabLayout.setOnTabSelectedListener(this);

        rlControls = (RelativeLayout) findViewById(R.id.rl_controls);

        List<String> filters = new ArrayList<>();
        filters.add(getString(R.string.txt_filters_sepia));
        filters.add(getString(R.string.txt_filters_blur));
        filters.add(getString(R.string.txt_filters_grayscale));
        filters.add(getString(R.string.txt_filters_edge));

        bitmapProcessor = new BitmapProcessor(this);
        bitmapProcessor.setBitmap(bitmap);

        bitmapSurfaceView = (BitmapSurfaceView) findViewById(R.id.bsv_after);
        bitmapSurfaceView.setBitmapProcessor(bitmapProcessor);

        spinner = (Spinner) findViewById(R.id.s_after);
        spinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, filters));
        spinner.setOnItemSelectedListener(this);

        sbIntensity = (SeekBar) findViewById(R.id.sb_intensity);
        sbIntensity.setOnSeekBarChangeListener(this);

        sepiaFilter = new GPUImageSepiaFilter(0);
        boxBlurFilter = new GPUImageBoxBlurFilter(0);
        grayscaleFilter = new GPUImageGrayscaleFilter();
        edgeDetection = new GPUImageSobelEdgeDetection();

        bitmapSurfaceView.addOnDoneListener(new BitmapSurfaceView.OnBitmapSurfaceListener() {
            @Override
            public void onBegin() {
                ready = false;
            }

            @Override
            public void onDone() {
                ready = true;
                if (dirty)
                    updateActivity();
            }
        });

        if (savedInstanceState == null) {
            filtersSettings = new FiltersSettings();
            updateActivity();
        } else {
            filtersSettings = savedInstanceState.getParcelable(STATE_FILTERS);
            TabLayout.Tab tab = tabLayout.getTabAt(savedInstanceState.getInt(STATE_TAB));
            if (tab != null)
                tab.select();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        dirty = true;
        updateActivity();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_filters, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_TAB, tabLayout.getSelectedTabPosition());
        outState.putParcelable(STATE_FILTERS, filtersSettings);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar.getId() == R.id.sb_intensity) {
            dirty = true;
            switch (spinner.getSelectedItemPosition()) {
                case FILTER_SEPIA:
                    filtersSettings.setSepia(progress);
                    break;
                case FILTER_BLUR:
                    filtersSettings.setBlur(progress);
                    break;
                case FILTER_GRAY:
                    break;
                case FILTER_EDGE:
                    break;
            }
            updateActivity();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.s_after) {
            dirty = true;
            updateActivity();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void updateControls() {
        switch (spinner.getSelectedItemPosition()) {
        case FILTER_SEPIA:
            sbIntensity.setProgress(filtersSettings.getSepia());
            sbIntensity.setVisibility(View.VISIBLE);
            sepiaFilter.setIntensity(filtersSettings.getSepia() / 100.0f);
            bitmapProcessor.setFilter(sepiaFilter);
            break;
        case FILTER_BLUR:
            sbIntensity.setProgress(filtersSettings.getBlur());
            sbIntensity.setVisibility(View.VISIBLE);
            boxBlurFilter.setBlurSize(filtersSettings.getBlur() / 100.0f);
            bitmapProcessor.setFilter(boxBlurFilter);
            break;
        case FILTER_GRAY:
            sbIntensity.setVisibility(View.INVISIBLE);
            bitmapProcessor.setFilter(grayscaleFilter);
            break;
        case FILTER_EDGE:
            sbIntensity.setVisibility(View.INVISIBLE);
            bitmapProcessor.setFilter(edgeDetection);
            break;
        }
    }

    private void updateActivity() {
        if (!ready)
            return;
        switch (tabLayout.getSelectedTabPosition()) {
        case TAB_BERORE_AND_AFTER:
            rlControls.setVisibility(View.VISIBLE);
            updateControls();
            bitmapProcessor.setSplit(true);
            break;
        case TAB_BERORE:
            rlControls.setVisibility(View.INVISIBLE);
            bitmapProcessor.setFilter(null);
            bitmapProcessor.setSplit(false);
            break;
        case TAB_AFTER:
            rlControls.setVisibility(View.VISIBLE);
            updateControls();
            bitmapProcessor.setSplit(false);
            break;
        }
        dirty = false;
        bitmapSurfaceView.redraw();
    }
}
