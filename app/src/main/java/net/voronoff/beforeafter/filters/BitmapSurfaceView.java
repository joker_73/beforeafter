package net.voronoff.beforeafter.filters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


public class BitmapSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    private static final String TAG = "BitmapSurfaceView";

    private BitmapProcessor bitmapProcessor;

    private OnBitmapSurfaceListener doneListener;

    private boolean dirty = false;
    private boolean inProgress = false;

    private int width = 0;
    private int height = 0;

    public BitmapSurfaceView(Context context) {
        super(context);
        init();
    }

    public BitmapSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BitmapSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        dirty = true;
        this.width = width;
        this.height = height;
        redraw();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    public void redraw() {
        if (inProgress || bitmapProcessor == null)
            return;

        inProgress = true;

        final SurfaceHolder holder = getHolder();
        final Canvas canvas = holder.lockCanvas(null);
        if (canvas == null) {
            Log.w(TAG, "canvas is null");
            inProgress = false;
            return;
        }
        if (doneListener != null)
            doneListener.onBegin();
        canvas.drawColor(Color.TRANSPARENT);
        bitmapProcessor.processAsync(new BitmapProcessor.OnBitmapReadyListener() {
            @Override
            public void ready(Bitmap bitmap) {
                if (bitmap != null)
                    canvas.drawBitmap(bitmap, 0f, 0f, new Paint());
                else
                    Log.w(TAG, "bitmap is null");
                holder.unlockCanvasAndPost(canvas);
                inProgress = false;
                if (dirty) {
                    dirty = false;
                    redraw();
                }
                if (doneListener != null)
                    doneListener.onDone();
            }
        }, width, height);
    }

    private void init() {
        setZOrderOnTop(true);
        SurfaceHolder holder = getHolder();
        holder.setFormat(PixelFormat.TRANSPARENT);
        holder.addCallback(this);
    }

    public void setBitmapProcessor(BitmapProcessor bitmapProcessor) {
        this.bitmapProcessor = bitmapProcessor;
    }

    public void addOnDoneListener(OnBitmapSurfaceListener doneListener) {
        this.doneListener = doneListener;
    }

    public interface OnBitmapSurfaceListener {
        void onBegin();
        void onDone();
    }
}
