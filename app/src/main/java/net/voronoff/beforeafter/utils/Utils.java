package net.voronoff.beforeafter.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;

public class Utils {
    private static final String TAG = "Utils";

    public static Bitmap decodeBitmap(String filePath, int width, int height) {
        if (width <= 0 || height <= 0) {
            Log.w(TAG, "wrong size");
            return null;
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(filePath, options);

        int scaleFactor = Math.min(options.outWidth / width, options.outHeight / height);

        options.inJustDecodeBounds = false;
        options.inSampleSize = scaleFactor;

        return BitmapFactory.decodeFile(filePath, options);
    }

    public static Bitmap decodeBitmapDisplaySize(Context context, String filePath) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        return decodeBitmap(filePath, point.x, point.y);
    }

    public static void addImageToGallery(Context context, String path) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(path);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }

    private Utils() { /* do nothing */ }
}