package net.voronoff.beforeafter.gallery;

import android.graphics.Bitmap;
import android.util.LruCache;

public class BitmapCache {

    private LruCache<String, Bitmap> lruCache;

    public BitmapCache(int part) {
        int maxMemory = (int) (Runtime.getRuntime().maxMemory());
        int cacheSize = maxMemory / (part > 0 ? part : 1);

        lruCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount();
            }
        };
    }

    public void put(String key, Bitmap bitmap) {
        if (key == null || bitmap == null) {
            return;
        }

        lruCache.put(key, bitmap);
    }

    public Bitmap get(String key) {
        return lruCache.get(key);
    }
}