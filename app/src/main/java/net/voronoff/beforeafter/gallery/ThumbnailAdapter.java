package net.voronoff.beforeafter.gallery;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import net.voronoff.beforeafter.R;
import net.voronoff.beforeafter.utils.Utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThumbnailAdapter extends CursorAdapter {

    private static final BitmapCache cache = new BitmapCache(4);
    private static final ExecutorService executorService = Executors.newFixedThreadPool(20);

    private final int imageWidth;
    private final int imageHeight;

    public ThumbnailAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);

        imageWidth = context.getResources().getDimensionPixelSize(R.dimen.thumbnail_width);
        imageHeight = context.getResources().getDimensionPixelSize(R.dimen.thumbnail_height);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.thumbnail, parent, false);

        ViewHolder viewHolder = new ViewHolder();

        viewHolder.imageView = (ImageView) view.findViewById(R.id.iv_thumbnail);
        viewHolder.progressBar = (ProgressBar) view.findViewById(R.id.pb_thumbnail);

        view.setTag(viewHolder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();

        int pathId = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        String path = cursor.getString(pathId);

        Bitmap bitmap = cache.get(path);
        resetImage(viewHolder);

        if (bitmap == null)
            loadImage(viewHolder, path);
        else
            setImage(viewHolder, bitmap);
    }

    private void resetImage(ViewHolder viewHolder) {
        if (viewHolder.task != null) {
            viewHolder.task.cancel(false);
            viewHolder.task = null;
        }

        viewHolder.imageView.setVisibility(View.INVISIBLE);
        viewHolder.progressBar.setVisibility(View.VISIBLE);
    }

    private void setImage(ViewHolder viewHolder, Bitmap bitmap) {
        viewHolder.imageView.setImageBitmap(bitmap);

        viewHolder.imageView.setVisibility(View.VISIBLE);
        viewHolder.progressBar.setVisibility(View.INVISIBLE);
    }

    private void loadImage(final ViewHolder viewHolder, final String path) {
        viewHolder.task = new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                return Utils.decodeBitmap(path, imageWidth, imageHeight);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                cache.put(path, bitmap);
                setImage(viewHolder, bitmap);
            }

            @Override
            protected void onCancelled(Bitmap bitmap) {
                super.onCancelled(bitmap);
                cache.put(path, bitmap);
            }
        }.executeOnExecutor(executorService);
    }

    private static class ViewHolder {
        public ImageView imageView;
        public ProgressBar progressBar;

        public AsyncTask<?, ?, ?> task;
    }
}