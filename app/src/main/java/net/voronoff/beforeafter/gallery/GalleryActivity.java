package net.voronoff.beforeafter.gallery;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import net.voronoff.beforeafter.R;
import net.voronoff.beforeafter.filters.FiltersActivity;
import net.voronoff.beforeafter.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GalleryActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = "GalleryActivity";

    private static final int CURSOR_LOADER_EXTERNAL = 0;
    private static final int CURSOR_LOADER_INTERNAL = 1;

    private static final int REQUEST_IMAGE_CAPTURE = 1;

    private static final String STATE_CAMERA_FILE_NAME = "STATE_CAMERA_FILE_NAME";
    private static final String STATE_GRID_VIEW        = "STATE_GRID_VIEW";

    private GridView gridView;
    private ThumbnailAdapter thumbnailAdapter;

    private String cameraFileName;
    private Parcelable gridViewState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {
                    File file = createImageFile();
                    cameraFileName = file.getAbsolutePath();
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                } catch (IOException e) {
                    Log.w(TAG, "Creating a file for photo is failed: " + e.getMessage());
                    return;
                }
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        thumbnailAdapter = new ThumbnailAdapter(this, null, 0);

        gridView = (GridView) findViewById(R.id.gv_table);
        gridView.setAdapter(thumbnailAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) thumbnailAdapter.getItem(position);
                int pathId = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                startFiltersActivity(cursor.getString(pathId));
            }
        });
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount)
                    fab.hide();
                else
                    fab.show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportLoaderManager().restartLoader(CURSOR_LOADER_EXTERNAL, null, this);
//        getSupportLoaderManager().restartLoader(CURSOR_LOADER_INTERNAL, null, this);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        cameraFileName = savedInstanceState.getString(STATE_CAMERA_FILE_NAME);
        gridViewState = savedInstanceState.getParcelable(STATE_GRID_VIEW);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_CAMERA_FILE_NAME, cameraFileName);
        if (gridViewState == null)
            outState.putParcelable(STATE_GRID_VIEW, gridView.onSaveInstanceState());
        else
            outState.putParcelable(STATE_GRID_VIEW, gridViewState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && cameraFileName != null) {
            startFiltersActivity(cameraFileName);

            // Some devices provide two copies of the image
            String lastImagePath = getLastImagePath();
            if (!cameraFileName.equals(lastImagePath) && isEqualImage(cameraFileName, lastImagePath)) {
                String where = MediaStore.Images.Media.DATA + "=?";
                String args[] = { lastImagePath };
                if (getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, where, args) == 0)
                    Log.w(TAG, "Deleting file is failed <" + lastImagePath + ">");
            }

            Utils.addImageToGallery(this, cameraFileName);

            cameraFileName = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gallery, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DATA,
        };
        CursorLoader cursorLoader = null;
        if (id == CURSOR_LOADER_EXTERNAL)
            cursorLoader = new CursorLoader(this, MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null);
        else if (id == CURSOR_LOADER_INTERNAL)
            cursorLoader = new CursorLoader(this, MediaStore.Images.Media.INTERNAL_CONTENT_URI, projection, null, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        thumbnailAdapter.swapCursor(data);
        if (gridViewState != null) {
            gridView.onRestoreInstanceState(gridViewState);
            gridViewState = null;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        thumbnailAdapter.swapCursor(null);
    }

    private String getLastImagePath() {
        String path = null;
        String[] columns = {
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DATA,
        };
        String orderBy = MediaStore.Images.Media._ID + " DESC";
        Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, orderBy);
        if(cursor != null) {
            if (cursor.moveToFirst()) {
                int pathId = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                path = cursor.getString(pathId);
            }
            cursor.close();
        }
        return path;
    }

    private void startFiltersActivity(String path) {
        if (path == null) {
            Log.w(TAG, "path is null");
            return;
        }

        Intent intent = new Intent(getApplicationContext(), FiltersActivity.class);
        intent.putExtra(FiltersActivity.ARG_PATH, path);
        startActivity(intent);
    }

    private File createImageFile() throws IOException {
        @SuppressLint("SimpleDateFormat")
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "BeforeAndAfter_" + timeStamp;
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists() && !storageDir.mkdir())
            Log.w(TAG, "Directory not created");
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    @SuppressWarnings("RedundantIfStatement")
    private boolean isEqualImage(String path1, String path2) {
        File f1 = new File(path1);
        File f2 = new File(path2);

        if (f1.length() != f2.length() || f1.lastModified() != f2.lastModified())
            return false;

        BitmapFactory.Options o1 = new BitmapFactory.Options();
        BitmapFactory.Options o2 = new BitmapFactory.Options();

        o1.inJustDecodeBounds = true;
        o2.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(path1, o1);
        BitmapFactory.decodeFile(path2, o2);

        if (o1.outWidth != o2.outWidth || o1.outHeight != o2.outHeight)
            return false;

        return true;
    }
}
